<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::middleware('auth:sanctum')->apiResource('/students',StudentController::class);

//Logout

Route::post('/logout',[\App\Http\Controllers\UserController::class,'logOut'])->middleware('auth:sanctum');

//Public routes

Route::post('/register',[\App\Http\Controllers\UserController::class,'registerUser']);
Route::post('/login',[\App\Http\Controllers\UserController::class,'login']);

//Route::apiResource('/students',StudentController::class);


