<?php

namespace App\Http\Controllers;

use App\Http\Requests\studentUpdateRequest;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {

        $students = Student::query();

        if ($request->get('search')) {
            $students = $students->where('name', 'like', "%{$request->get('search')}%")
                ->orWhere('city', 'like', "%{$request->get('search')}%");
        }

       // dd($students->toSql());
        $students = $students->paginate(20);

        return $students;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'fees' => 'required',
        ]);
        return Student::create([
            'name' => $request->get('name'),
            'city' => $request->get('city'),
            'fees' => $request->get('fees'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return Student
     */
    public function show(Student $student): Student
    {
        return $student;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Student $student
     * @return Student
     */
    public function update(studentUpdateRequest $request, Student $student)
    {

        $student->update([
            'name' => $request->get('name'),
            'city' => $request->get('city'),
            'fees' => $request->get('fees'),

        ]);

        return $student;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     * @return bool
     */
    public function destroy(Student $student)
    {
        return $student->delete();
    }
}
