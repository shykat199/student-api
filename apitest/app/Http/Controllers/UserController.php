<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function registerUser(UserRequest $request)
    {

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = $user->createToken('myToken')->plainTextToken;
        return response([
            'user' => $user,
            'token' => $token
        ], 201);

    }


    public function login(UserLoginRequest $request){

        $user=User::where('email',$request->email)->first();

        if(!$user || !Hash::check($request->get('password'),
            $user->password)){

            return response([
                'message'=>"Incorrect Credential",
            ],401);
        }

        $token = $user->createToken('myToken')->plainTextToken;
        return response([
            'user' => $user,
            'token' => $token
        ],200);

    }

    public function  logOut(){
        auth()->user()->tokens()->delete();
        return response([
            'message'=>"Successfully Logout"
        ]);
    }
}
